import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Model from '@ckeditor/ckeditor5-ui/src/model';
import Collection from '@ckeditor/ckeditor5-utils/src/collection';
import Link from '@ckeditor/ckeditor5-link/src/link';
import { addListToDropdown, createDropdown } from '@ckeditor/ckeditor5-ui/src/dropdown/utils';
import WordCount from '@ckeditor/ckeditor5-word-count/src/wordcount';

function addDropdownItemsToItemsCollection(items, dropdowns) {
    dropdowns.forEach(function (item) {
        items.add({
            type: "button",
            model: new Model({
                withText: true,
                label: item.label,
                value: item.value
            })
        });
    });

    return items;
}

class InsertCourseVariables extends Plugin {
    init() {
        const editor = this.editor;
        editor.ui.componentFactory.add("InsertCourseVariables", locale => {
            const dropdownView1 = createDropdown(locale);
            dropdownView1.buttonView.set({
                withText: true,
                label: "Add course variables",
                tooltip: true
            });
            //
            let items = new Collection();
            let dropdowns = editor.config.get('courseVariables');
            items = addDropdownItemsToItemsCollection(items, dropdowns);
            addListToDropdown(dropdownView1, items);
            dropdownView1.on('execute', evt => {
                editor.model.change(writer => {
                    editor.model.insertContent(writer.createText(evt.source.value + " "));
                });
            });
            return dropdownView1;
        });

    }
}

class InsertInstitutionVariables extends Plugin {
    init() {
        const editor = this.editor;
        editor.ui.componentFactory.add("InsertInstitutionVariables", locale => {
            const dropdownView2 = createDropdown(locale);
            dropdownView2.buttonView.set({
                withText: true,
                label: "Add institution variables",
                tooltip: true
            });
            //
            let items = new Collection();
            let dropdowns = editor.config.get('institutionVariables');
            items = addDropdownItemsToItemsCollection(items, dropdowns);
            addListToDropdown(dropdownView2, items);
            dropdownView2.on('execute', evt => {
                editor.model.change(writer => {
                    editor.model.insertContent(writer.createText(evt.source.value + " "));
                });
            });
            return dropdownView2;
        });

    }
}

class InsertIssuerVariables extends Plugin {
    init() {
        const editor = this.editor;
        editor.ui.componentFactory.add("InsertIssuerVariables", locale => {
            const dropdownView3 = createDropdown(locale);
            dropdownView3.buttonView.set({
                withText: true,
                label: "Add issuer variables",
                tooltip: true
            });
            //
            let items = new Collection();
            let dropdowns = editor.config.get('issuerVariables');
            items = addDropdownItemsToItemsCollection(items, dropdowns);
            addListToDropdown(dropdownView3, items);
            dropdownView3.on('execute', evt => {
                editor.model.change(writer => {
                    editor.model.insertContent(writer.createText(evt.source.value + " "));
                });
            });
            return dropdownView3;
        });

    }
}

class InsertRecipientVariables extends Plugin {
    init() {
        const editor = this.editor;
        editor.ui.componentFactory.add("InsertRecipientVariables", locale => {
            const dropdownView4 = createDropdown(locale);
            dropdownView4.buttonView.set({
                withText: true,
                label: "Add recipient variables",
                tooltip: true
            });
            //
            let items = new Collection();
            let dropdowns = editor.config.get('recipientVariables');
            items = addDropdownItemsToItemsCollection(items, dropdowns);
            addListToDropdown(dropdownView4, items);
            dropdownView4.on('execute', evt => {
                editor.model.change(writer => {
                    editor.model.insertContent(writer.createText(evt.source.value + " "));
                });
            });
            return dropdownView4;
        });

    }
}

class InsertCertificateVariables extends Plugin {
    init() {
        const editor = this.editor;
        editor.ui.componentFactory.add("InsertCertificateVariables", locale => {
            const dropdownView5 = createDropdown(locale);
            dropdownView5.buttonView.set({
                withText: true,
                label: "Add certificate variables",
                tooltip: true
            });

            let items = new Collection();
            let dropdowns = editor.config.get('certificateVariables');
            items = addDropdownItemsToItemsCollection(items, dropdowns);

            addListToDropdown(dropdownView5, items);
            dropdownView5.on('execute', evt => {
                editor.model.change(writer => {
                    editor.model.insertContent(writer.createText(evt.source.value + " "));
                });
            });
            return dropdownView5;
        });

    }
}

ClassicEditor
    .create(document.querySelector('#emailTemplate'), {
        plugins: [WordCount, Essentials, Paragraph, Bold, Italic, Underline, Link, InsertCertificateVariables, InsertRecipientVariables, InsertCourseVariables, InsertInstitutionVariables, InsertIssuerVariables],
        toolbar: ['bold', 'italic', 'underline', 'link', 'InsertCertificateVariables', 'InsertRecipientVariables', 'InsertCourseVariables', 'InsertInstitutionVariables', 'InsertIssuerVariables'],
        certificateVariables: [
            {
                label: 'Certificate Name',
                value: '{{certificateName}}'
            },
            {
                label: 'QR Code',
                value: '{{certQrCode}}'
            },
            {
                label: 'Revocation Reason',
                value: '{{revocationReason}}'
            },
        ],
        recipientVariables: [
            {
                label: 'Recipient Name',
                value: '{{recipientName}}'
            },
        ],
        courseVariables: [
            {
                label: 'Course Code',
                value: '{{courseCode}}'
            },
            {
                label: 'Course Title',
                value: '{{courseTitle}}'
            },
            {
                label: 'Course Description',
                value: '{{courseDescription}}'
            }
        ],
        institutionVariables: [
            {
                label: 'Institution Name',
                value: '{{institutionName}}'
            },
            {
                label: 'Institution Website',
                value: '{{institutionWebsite}}'
            },
            {
                label: 'Institution Telephone',
                value: '{{institutionTelephone}}'
            },
            {
                label: 'Institution Logo',
                value: '{{institutionLogo}}'
            },
            {
                label: 'Institution Address',
                value: '{{institutionAddress}}'
            },
            {
                label: 'Institution Zipcode',
                value: '{{institutionZipcode}}'
            }
        ],
        issuerVariables: [
            {
                label: 'Issuer Name',
                value: '{{issuerName}}'
            },
            {
                label: 'Issuer Website',
                value: '{{issuerWebsite}}'
            },
            {
                label: 'Issuer Telephone',
                value: '{{issuerTelephone}}'
            },
            {
                label: 'Issuer Logo',
                value: '{{issuerLogo}}'
            },
            {
                label: 'Issuer Address',
                value: '{{issuerAddress}}'
            },
            {
                label: 'Issuer Zipcode',
                value: '{{issuerZipcode}}'
            },
        ]
    })
    .then(editor => {
        window.emailTemplateEditor = editor;
        const wordCountPlugin = editor.plugins.get('WordCount');
        const wordCountWrapper = document.getElementById('word-count');

        wordCountWrapper.appendChild(wordCountPlugin.wordCountContainer);
    })
    .catch(error => {
        // console.error(error);
    });

ClassicEditor
    .create(document.querySelector('#edit_emailTemplate'), {
        plugins: [WordCount, Essentials, Paragraph, Bold, Italic, Underline, Link, InsertCertificateVariables, InsertRecipientVariables, InsertCourseVariables, InsertInstitutionVariables, InsertIssuerVariables],
        toolbar: ['bold', 'italic', 'underline', 'link', 'InsertCertificateVariables', 'InsertRecipientVariables', 'InsertCourseVariables', 'InsertInstitutionVariables', 'InsertIssuerVariables'],
        certificateVariables: [
            {
                label: 'Certificate Name',
                value: '{{certificateName}}'
            },
            {
                label: 'QR Code',
                value: '{{certQrCode}}'
            },
            {
                label: 'Revocation Reason',
                value: '{{revocationReason}}'
            },
        ],
        recipientVariables: [
            {
                label: 'Recipient Name',
                value: '{{recipientName}}'
            },
        ],
        courseVariables: [
            {
                label: 'Course Code',
                value: '{{courseCode}}'
            },
            {
                label: 'Course Title',
                value: '{{courseTitle}}'
            },
            {
                label: 'Course Description',
                value: '{{courseDescription}}'
            }
        ],
        institutionVariables: [
            {
                label: 'Institution Name',
                value: '{{institutionName}}'
            },
            {
                label: 'Institution Website',
                value: '{{institutionWebsite}}'
            },
            {
                label: 'Institution Telephone',
                value: '{{institutionTelephone}}'
            },
            {
                label: 'Institution Logo',
                value: '{{institutionLogo}}'
            },
            {
                label: 'Institution Address',
                value: '{{institutionAddress}}'
            },
            {
                label: 'Institution Zipcode',
                value: '{{institutionZipcode}}'
            }
        ],
        issuerVariables: [
            {
                label: 'Issuer Name',
                value: '{{issuerName}}'
            },
            {
                label: 'Issuer Website',
                value: '{{issuerWebsite}}'
            },
            {
                label: 'Issuer Telephone',
                value: '{{issuerTelephone}}'
            },
            {
                label: 'Issuer Logo',
                value: '{{issuerLogo}}'
            },
            {
                label: 'Issuer Address',
                value: '{{issuerAddress}}'
            },
            {
                label: 'Issuer Zipcode',
                value: '{{issuerZipcode}}'
            },
        ]
    })
    .then(editor => {
        window.editEmailTemplateEditor = editor;

        const wordCountPlugin = editor.plugins.get('WordCount');
        const wordCountWrapper = document.getElementById('word-count');

        wordCountWrapper.appendChild(wordCountPlugin.wordCountContainer);
    })
    .catch(error => {
        // console.error(error);
    });
